package ru.t1.sarychevv.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;
import ru.t1.sarychevv.tm.dto.response.user.UserLoginResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpointClient {

    @NotNull UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull UserProfileResponse profile(@NotNull UserProfileRequest request);

}
