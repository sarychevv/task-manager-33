package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.project.ProjectGetByIndexRequest;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(index);
        @Nullable final Project project = getProjectEndpoint().getProjectByIndex(request).getProject();
        showProject(project);
    }

}
