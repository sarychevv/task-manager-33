package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "change-user-password";
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
