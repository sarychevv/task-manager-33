package ru.t1.sarychevv.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerAboutResponse;
import ru.t1.sarychevv.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

}
