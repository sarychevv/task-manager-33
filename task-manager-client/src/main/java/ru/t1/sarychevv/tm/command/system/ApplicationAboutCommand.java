package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerAboutResponse;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getServiceLocator().getSystemEndpointClient().getAbout(new ApplicationAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

}
