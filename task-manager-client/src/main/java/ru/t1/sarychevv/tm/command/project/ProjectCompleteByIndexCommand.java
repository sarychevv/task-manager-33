package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(index, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

}
