package ru.t1.sarychevv.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.data.DataBackupLoadRequest;
import ru.t1.sarychevv.tm.enumerated.Role;

public class DataBackupSaveCommand extends DataBase64SaveCommand {

    @NotNull
    public static final String DESCRIPTION = "Save backup to file";

    @NotNull
    public static final String NAME = "backup-save";

    @SneakyThrows
    @Override
    public void execute() {
        serviceLocator.getDomainEndpointClient().loadDataBackup(new DataBackupLoadRequest());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
