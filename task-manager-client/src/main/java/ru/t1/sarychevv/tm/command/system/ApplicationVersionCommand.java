package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.response.system.ServerVersionResponse;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionResponse response = getServiceLocator().getSystemEndpointClient().getVersion(new ApplicationVersionRequest());
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show program version.";
    }

}
