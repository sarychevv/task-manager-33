package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.user.UserRemoveRequest;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "User remove";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
