package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(firstName, lastName, middleName);
        getUserEndpoint().updateUserProfile(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
