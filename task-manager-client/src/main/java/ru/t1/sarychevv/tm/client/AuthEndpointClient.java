package ru.t1.sarychevv.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;
import ru.t1.sarychevv.tm.dto.response.user.UserLoginResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
