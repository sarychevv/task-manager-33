package ru.t1.sarychevv.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public class TerminalUtil {

    @NotNull
    static Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
