package ru.t1.sarychevv.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @Nullable
    private static final String ARGUMENT = null;

    @NotNull
    private static final String DESCRIPTION = "Connect to server";

    @NotNull
    private static final String NAME = "connect";

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpointClient().connect();
        final Socket socket = getServiceLocator().getAuthEndpointClient().getSocket();
        getServiceLocator().getProjectEndpointClient().setSocket(socket);
        getServiceLocator().getTaskEndpointClient().setSocket(socket);
        getServiceLocator().getDomainEndpointClient().setSocket(socket);
        getServiceLocator().getSystemEndpointClient().setSocket(socket);
        getServiceLocator().getUserEndpointClient().setSocket(socket);
    }

    @Override
    public @Nullable String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

