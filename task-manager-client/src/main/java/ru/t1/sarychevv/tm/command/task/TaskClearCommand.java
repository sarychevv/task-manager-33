package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.task.TaskClearRequest;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTask(new TaskClearRequest());
    }

}
