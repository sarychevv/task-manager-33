package ru.t1.sarychevv.tm.dto.request.user;

import lombok.NoArgsConstructor;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class UserProfileRequest extends AbstractUserRequest {
}
