package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

}
