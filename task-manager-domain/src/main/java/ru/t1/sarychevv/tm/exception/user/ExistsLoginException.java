package ru.t1.sarychevv.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! User with this login already exists...");
    }

}
