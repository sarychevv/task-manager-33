package ru.t1.sarychevv.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskGetByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}
