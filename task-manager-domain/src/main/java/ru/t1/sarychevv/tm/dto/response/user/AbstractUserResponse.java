package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;
import ru.t1.sarychevv.tm.model.User;

@NoArgsConstructor
public class AbstractUserResponse extends AbstractResultResponse {

    @Getter
    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
