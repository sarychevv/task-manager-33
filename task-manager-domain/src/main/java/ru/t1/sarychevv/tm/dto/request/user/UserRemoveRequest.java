package ru.t1.sarychevv.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveRequest(@Nullable final String login) {
        this.login = login;
    }

}
