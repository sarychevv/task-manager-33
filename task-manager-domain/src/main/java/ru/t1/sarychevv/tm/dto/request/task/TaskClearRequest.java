package ru.t1.sarychevv.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearRequest extends AbstractUserRequest {
}
