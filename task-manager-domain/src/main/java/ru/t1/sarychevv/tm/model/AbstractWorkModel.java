package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractWorkModel extends AbstractModel {

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

}
