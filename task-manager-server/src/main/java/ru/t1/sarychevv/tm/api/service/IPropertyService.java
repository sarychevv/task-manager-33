package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();
}
