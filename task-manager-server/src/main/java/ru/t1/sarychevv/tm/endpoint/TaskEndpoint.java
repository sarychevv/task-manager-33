package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.api.service.IProjectTaskService;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.api.service.ITaskService;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.response.task.*;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.model.Task;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @Override
    @NotNull
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task = getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().removeAll(userId);
        return new TaskClearResponse();
    }

    @Override
    @NotNull
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskListByProjectIdResponse getTaskByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final TaskSort sort = request.getTaskSort();
        @Nullable final List<Task> tasks;
        if (sort == null) tasks = getTaskService().findAll(userId);
        else tasks = getTaskService().findAll(userId, sort.getComparator());
        return new TaskListResponse(tasks);
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().removeOneById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().removeOneByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
