package ru.t1.sarychevv.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.api.service.IUserService;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.exception.user.AccessDeniedException;
import ru.t1.sarychevv.tm.model.User;

public class AbstractEndpoint {

    @Getter
    protected IServiceLocator serviceLocator;

    protected void check(@Nullable final AbstractUserRequest request,
                         @Nullable final Role role) {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        if (!(roleUser == role)) throw new AccessDeniedException();

    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
