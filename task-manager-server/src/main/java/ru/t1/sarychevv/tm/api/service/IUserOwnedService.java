package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.IUserOwnedRepository;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable M updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}

