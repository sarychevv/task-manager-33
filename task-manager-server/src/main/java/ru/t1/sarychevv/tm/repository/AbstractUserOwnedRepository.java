package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.IUserOwnedRepository;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void removeAll(@NotNull final String userId) {
        removeAll(findAll(userId));
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .filter(r -> id.equals(r.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);

    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) records
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .count();
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        add(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOne(@NotNull final String userId, @NotNull final M model) {
        return removeOneById(userId, model.getId());
    }

}
