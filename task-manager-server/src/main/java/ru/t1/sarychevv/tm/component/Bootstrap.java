package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.*;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.dto.request.data.*;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.endpoint.*;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.repository.ProjectRepository;
import ru.t1.sarychevv.tm.repository.TaskRepository;
import ru.t1.sarychevv.tm.repository.UserRepository;
import ru.t1.sarychevv.tm.service.*;
import ru.t1.sarychevv.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private final ICalcEndpoint calcEndpoint = new CalcEndpoint(this);

    {
        registry(calcEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();;
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    public void start() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initDemoData();
    }

    public void stop() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
    }

    private void initDemoData() {
        @NotNull final User userCustom = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userCustom.getId(), "PROJECT1", "Project1 for userCustom");
        projectService.create(userCustom.getId(), "PROJECT2", "Project2 for userCustom");
        projectService.create(userCustom.getId(), "PROJECT3", "Project3 for userCustom");
        projectService.create(userAdmin.getId(), "PROJECT4", "Project4 for userAdmin");
        projectService.create(userAdmin.getId(), "PROJECT5", "Project5 for userAdmin");

        taskService.create(userCustom.getId(), "TASK1", "Task1 for userCustom");
        taskService.create(userCustom.getId(), "TASK2", "Task2 for userCustom");
        taskService.create(userCustom.getId(), "TASK3", "Task3 for userCustom");
        taskService.create(userCustom.getId(), "TASK4", "Task4 for userCustom");
        taskService.create(userAdmin.getId(), "TASK5", "Task5 for userAdmin");
        taskService.create(userAdmin.getId(), "TASK6", "Task6 for userAdmin");
    }

}
